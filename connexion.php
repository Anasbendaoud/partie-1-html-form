<?php

  $servername="localhost" ; 
  $username="root" ; 
  $password="" ; 
  $dbname="finder" ; 
  
  // Connexion à la base de données try {
  try {
    $db=new PDO("mysql:host=$servername;dbname=$dbname", $username, $password); 
  // Avtiver PDO error en mode exception
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
  } catch(PDOException $e) {
  echo "Connexion échouée..." . $e->getMessage();
  }
    

  