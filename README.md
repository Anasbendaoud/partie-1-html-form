______________________________________________________________________________________________________________________________________________

                                             Partie 1  – HTML form
______________________________________________________________________________________________________________________________________________


    - réaliser une base de données finder avec une table user, colonnes email, mot de passe
    - insérer quelques utilisateurs
    - réaliser un formulaire email/mdp sur connexion.html
    - vérifier le couple email/mdp sur une page verification.php
    - rediriger l’utilisateur sur le formulaire en cas d’échec avec un message lui indiquant que son login ou son mot de passe ne sont pas bon.
    - utiliser la fonction aes_encrypt pour encoder le mot de passe
      https://mariadb.com/kb/en/aes_encrypt/
