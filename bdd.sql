-- Active: 1663174127323@@127.0.0.1@3306@finder

--Création de la base de données
CREATE DATABASE IF NOT EXISTS finder CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- Création de la table base de données
CREATE TABLE IF NOT EXISTS user( 
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL, 
    email VARCHAR(255) NOT NULL, 
    password BLOB NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--Remlissage de la table
INSERT INTO user (id, email, password) 
        VALUES (1, 'anas@gmail.com', AES_ENCRYPT('anas123', SHA2('password',512)));
INSERT INTO user (id, email, password) 
        VALUES (2, 'btssio@esiee-it.fr',AES_ENCRYPT('btssio123',SHA2('section',512)));
INSERT INTO user (id, email, password) 
        VALUES (3, 'slam2@esiee-it.fr',AES_ENCRYPT('slam2123',SHA2('option',512)));
INSERT INTO user (id, email, password) 
        VALUES (4, 'mbape@outlook.com',AES_ENCRYPT('mbape123',SHA2('foot',512)));

